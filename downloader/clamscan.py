import requests, time, xlsxwriter, os, re, shutil, operator, glob
from django.core.mail import send_mail

import subprocess
email="letzlerr@gao.gov"
project_path="/var/docket_process_files/OCC-2013-0003/*"
original_files = glob.glob(project_path)

quarantine_path = os.path.normpath(os.path.join(project_path[:-2],"flagged_by_clam_AV/*"))
os.makedirs(quarantine_path[:-2],exist_ok=True)
print(os.path.join(project_path[:-2],"antivirus_scan.log"))
subprocess.run(["clamscan", project_path, "--recursive", "--move="+quarantine_path[:-2], "--log="+os.path.join(project_path[:-2],"antivirus_scan.log")])
post_scan_files = glob.glob(project_path)
quarantine_files = glob.glob(quarantine_path)
print(quarantine_files)
if quarantine_files==[]:
            #number can grow if we add an antivirus log; or stay the same if the antivirus log already existed or could not be created.  that is not worrying
            assert len(post_scan_files)>=len(original_files), "The number of files dropped during the virus scan, but no viruses were quarantined.  Something's odd!"
else:
            print(str(['File(s) in your docket download flagged as potential viruses', 'clamAV flagged files in your docket download and moved them to ' + quarantine_path[:-2] + "\n Rob Letzler in ARM has been notified and will investigate. The following files were quarantined and not included in your ZIP file:  " +str(quarantine_files), 'letzlerr@gao.gov', [email, "letzlerr@gao.gov"]]))
            #send_mail('File(s) in your docket download flagged as potential viruses', 'clamAV flagged files in your docket download and moved them to ' + quarantine_path + "\n Rob Letzler in ARM has been notified and will investigate. The following files were quarantined and not included in your ZIP file:  " +str(quarantine_files), 'letzlerr@gao.gov', [email, "letzlerr@gao.gov"], fail_silently=False)
