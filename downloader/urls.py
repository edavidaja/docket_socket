from django.conf.urls import url
#, patterns, include
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^results/(.*)$', views.results_zip, name='results_zip'),
]
