# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-25 19:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('downloader', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='docket',
            name='email',
            field=models.CharField(default='test', max_length=64),
            preserve_default=False,
        ),
    ]
